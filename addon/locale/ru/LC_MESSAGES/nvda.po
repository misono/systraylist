# SystrayList Portuguese Translation
# Copyright (C) 2012 Rui Rui Batista & Rui Fontes
# This file is distributed under the same license as the systrayList package.
# Rui Batista <ruiandrebatista@gmail.com, 2012
msgid ""
msgstr ""
"Project-Id-Version: 1.0\n"
"Report-Msgid-Bugs-To: nvda-translations@freelists.org\n"
"POT-Creation-Date: 2013-01-06 12:47+0100\n"
"PO-Revision-Date: 2013-02-09 20:58+0400\n"
"Last-Translator: Bernd Dorer <bernd_dorer@yahoo.de>\n"
"Language-Team: German <bernd_dorer@yahoo.de>\n"
"Language: Portuguese\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

msgid "System Tray List"
msgstr "Список системного трея"

msgid "Icons on the System Tray:"
msgstr "Значки на системном трее"

msgid "Task Bar List"
msgstr "Список значков на панеле задач"

msgid "Icons of running applications in the task bar"
msgstr "Значки запущеных приложений на панеле задач"

#. Documentation
msgid ""
"Shows the list of buttons on the System Tray. If pressed twice quickly, "
"shows the items on the task bar."
msgstr ""
"Показывает список значков на системном трее. Если нажать дважды быстро, "
"показывает элементы на панеле задач."

msgid "&Left Click"
msgstr "Левый клик"

msgid "Left &Double Click"
msgstr "двойной левый клик"

msgid "&Right Click"
msgstr "правый клик"

msgid ""
" {name} is a free add-on for NVDA.\n"
"You can make a donation to its author to support further developments of "
"this add-on and other free software products.\n"
"Do you want to donate now? (you will be redirected to the Paypal website)."
msgstr ""
" {name} является бесплатным дополнением для NVDA.\n"
"Вы можете сделать пожертвование автору для поддержки Дальнейшего развития "
"этого дополнения и других бесплатных программных продуктов.\n"
"Вы хотите пожертвовать сейчас? ;(Вы будете перенаправлены на сайт Paypal)."

msgid "Request for Contributions to {name}"
msgstr "Запрос Взноса для {name}"

#. Add-on description
#. TRANSLATORS: Summary for this add-on to be shown on installation and add-on informaiton.
#, fuzzy
msgid "sysTrayList - list sys tray and task bar elements"
msgstr "Список значков на системном трее и на панеле задач"

#. Add-on description
#. Translators: Long description to be shown for this add-on on installation and add-on information
msgid ""
"Shows the list of buttons on the System Tray with NVDA+F11 once, twice shows "
"running task lists."
msgstr ""
"Показывает список значков на системном трее. Если нажать дважды быстро, "
"показывает элементы на панеле задач."

#~ msgid "Shows the list of buttons on the System Tray"
#~ msgstr "Показывает список кнопок на системном трее"

#~ msgid "Shows the list of icons in the task bar"
#~ msgstr "Показывает список значков на системном трее"
